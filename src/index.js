import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import SplashScreen from './screens/Splash';
import HomeScreen from './screens/Home';
import CardScreen from './screens/Card';

const MainNavigator = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: {
      headerShown: false
    }
  },
  CardScreen: {
    screen: CardScreen,
    navigationOptions: {
      headerShown: false
    }
  }
});

const Application = createAppContainer(createSwitchNavigator(
  {

    SplashScreen: {
      screen: SplashScreen,
      navigationOptions: {
        headerShown: false
      }
    },App: MainNavigator
  },
  {
    initialRouteName: 'SplashScreen',
    backBehavior: 'App'
  }
));


export default Application;