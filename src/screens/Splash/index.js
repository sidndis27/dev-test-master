import React, { Component } from 'react';
import { Image, View,Text } from 'react-native';
import styles from "./style"

export default class SplashScreen extends Component {
    
    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.navigate('App');
        }, 2000);
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={{color:'#fff',fontSize:20}}>
                    Welcome to Dev Test App
                </Text>
                      </View>
        );
    }
}