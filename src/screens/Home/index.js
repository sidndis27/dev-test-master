import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity } from 'react-native';
import styles from "./style"

export default class HomeScreen extends Component {

    state = {
        username: ''
    }

    onChangeText(text) {
        this.setState({ username: text });
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <TextInput
                    style={{ height: 40, borderColor: '#ffffff', borderWidth: 1, width:200, color:'#ffffff', padding: 10 }}
                    onChangeText={text => this.onChangeText(text)}
                    value={this.state.username}
                    placeholder={'Enter username'}
                    placeholderTextColor={'#ffffff'}
                />
                <TouchableOpacity style={styles.primaryButton} onPress={() => { navigate('CardScreen') }}>
                    <Text style={styles.primaryButtonText}>Submit</Text>
                </TouchableOpacity>
            </View>
        );
    }
}