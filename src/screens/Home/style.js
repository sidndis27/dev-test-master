import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'#000'
  },
  primaryButton: {
    margin:20,
    backgroundColor: '#ffffff',
    borderRadius: 10,
    paddingBottom: 18,
    paddingTop: 18,
    width: 200
  },
  primaryButtonText: {
    fontSize: 16,
    color: '#000000',
    width:'100%',
    fontWeight: 'bold',
    textAlign:'center',
    
  },
})

export default styles
