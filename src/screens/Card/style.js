import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'#fff'
  },
  primaryButton: {
    backgroundColor: '#000',
    borderRadius: 10,
    paddingBottom: 18,
    paddingTop: 18,
    width: 200
  },
  primaryButtonText: {
    fontSize: 16,
    color: '#fff',
    width:'100%',
    fontWeight: 'bold',
    textAlign:'center',
    
  },
})

export default styles
