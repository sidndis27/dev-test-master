import React, { Component } from 'react';
import { Image, View, Text, TextInput, TouchableOpacity } from 'react-native';
import styles from "./style"
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';


export default class CardScreen extends Component {

    state = {
        title: '',
        text: '',
        avatarSource: require('../../asserts/images/placeholder.png'),
    }

    onChangeText(type, text) {
        if (type == 1) {
            this.setState({ title: text });
        } else {
            this.setState({ text: text });
        }

    }

    createFormData(photo, body) {
        console.log("here");
        const data = new FormData();
      
        data.append("media", {
          name: photo.fileName,
          type: photo.type,
          uri: photo.uri
        });
      
        Object.keys(body).forEach(key => {
          data.append(key, body[key]);
        });
      
        return data;
      }

    handleUploadPhoto () {
        console.log("Start");
        fetch("http://192.168.43.138:8888/backend/save.php", {
          method: "POST",
          body: this.createFormData(this.state.avatarSource, { userName: "sid", title: "abc", text: "hello" })
        })
          .then(response => {
              console.log("res",response);
            // response.json()
          })
          .then(response => {
            console.log("upload succes", response);
            alert("Upload success!");
          })
          .catch(error => {
            console.log("upload error", error);
            alert("Upload failed!");
          });
      };

    render() {
        const options = {
            title: 'Select Avatar',
            customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        return (
            <View style={styles.container}>
                <TextInput
                    style={{ height: 40, borderColor: '#000', borderWidth: 1, width: 200, color: '#000', padding: 10 }}
                    onChangeText={text => this.onChangeText(1, text)}
                    value={this.state.title}
                    placeholder={'Enter title'}
                    placeholderTextColor={'#000'}
                />
                <TextInput
                    multiline
                    numberOfLines={4}
                    style={{ height: 100, borderColor: '#000', borderWidth: 1, width: 200, color: '#000', padding: 10, marginTop: 10 }}
                    onChangeText={text => this.onChangeText(2, text)}
                    value={this.state.text}
                    placeholder={'Enter Text'}
                    placeholderTextColor={'#000'}
                />
                {/* require('../../asserts/images/placeholder.png') */}
                <Image source={this.state.avatarSource} style={{ width: 200, height: 200, margin: 20 }} />
                <TouchableOpacity style={styles.primaryButton} onPress={() => {
                    ImagePicker.launchCamera(options, (response) => {
                        if (response.didCancel) {
                            console.log('User cancelled image picker');
                        } else if (response.error) {
                            console.log('ImagePicker Error: ', response.error);
                        } else if (response.customButton) {
                            console.log('User tapped custom button: ', response.customButton);
                        } else {
                            // const source = { uri: response.uri };
                            ImageResizer.createResizedImage(this.state.image.uri, 500, 500, 'JPEG', 70).then((response) => {
                                const source = { uri: response.uri };
                                this.setState({
                                    avatarSource: source,
                                });
                                // response.uri is the URI of the new image that can now be displayed, uploaded...
                                // response.path is the path of the new image
                                // response.name is the name of the new image with the extension
                                // response.size is the size of the new image
                              }).catch((err) => {
                                // Oops, something went wrong. Check that the filename is correct and
                                // inspect err to get more details.
                              });
                            
                        }
                    });
                }}>
                    <Text style={styles.primaryButtonText}>Capture Image</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.primaryButton} onPress={() => this.handleUploadPhoto()}>
                    <Text style={styles.primaryButtonText}>Upload</Text>
                </TouchableOpacity>
            </View>
        );
    }
}